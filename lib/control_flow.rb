# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  #This works perfectly fine, but after looking at the solutions, I see there
  #is an easier way to do it using each_char so I don't waste time creating an
  #unnecessary array, so I created a better way below my original solution
  # My original solution:
  # alpha_lower = ""
  # ("a".."z").each { |char| alpha_lower += char}
  # str.delete!(alpha_lower)
  # return str

  #Note: The solution that was provided is incorrect, so this is adapted
  str.each_char {|ch| str.delete!(ch) if ch != ch.upcase}
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  last_inx = str.length - 1
  if str.length % 2 == 0
    return str[last_inx /2] + str[(last_inx / 2) + 1]
  else
    return str[last_inx / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  count = 1
  factorial = 1
  while count <= num
    factorial *= count
    count += 1
  end
  return factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  idx = 0
  str = ""
  while idx < arr.length
    str += arr[idx].to_s
    str += separator if idx != arr.length - 1
    idx += 1
  end

  return str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  count = 0
  while count < str.length
    if count % 2 == 1
      str[count] = str[count].upcase
    else
      str[count] = str[count].downcase
    end
    count += 1
  end
  return str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split
  arr.each_index do |i|
    if arr[i].length >= 5
      arr[i].reverse!
    end
  end

  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  count = 1
  arr = []
  while count <= n
    if count % 3 == 0 && count % 5 == 0
      arr << "fizzbuzz"
    elsif count % 3 == 0
      arr << "fizz"
    elsif count % 5 == 0
      arr << "buzz"
    else
      arr << count
    end
    count += 1
  end

  return arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  newarr = []
  count = arr.length - 1
  idx = 0

  while count >= 0
    newarr[idx] = arr[count]
    idx += 1
    count -= 1
  end

  return newarr
end
#Here is the solution provided, that is a little cleaner:
# def my_reverse(arr)
#   reversed = []
#
#   arr.each do |el|
#    reversed.unshift(el)
#   end
#
#   reversed
# end


# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  count = 1
  factors = 0
  while count <= num
     factors += 1 if num % count == 0
     count += 1
   end

   return true if factors == 2
   return false if factors != 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = []
  count = 1

  while count <= num
    arr << count if num % count == 0
    count += 1
  end
  return arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = factors(num)
  newarr = []
  arr.each { |ele| newarr.push(ele) if prime?(ele)}
  newarr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |ele|
    evens << ele if ele.even?
    odds << ele if ele.odd?
  end

  return evens[0] if evens.length == 1
  return odds[0] if odds.length == 1
end

#Small change so that I can try to resubmit
